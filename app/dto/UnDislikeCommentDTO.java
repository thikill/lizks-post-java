package dto;

/**
 * Created by Administrator on 11/13/2015.
 */
public class UnDislikeCommentDTO {
    private  String userID;
    private  String commentID;
    private  String commentUserID;
    public String getCommentUserID() {
        return commentUserID;
    }

    public void setCommentUserID(String commentUserID) {
        this.commentUserID = commentUserID;
    }



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCommentID() {
        return commentID;
    }

    public void setCommentID(String commentID) {
        this.commentID = commentID;
    }
}
