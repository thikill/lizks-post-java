package dto;

import model.UserUDT;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Administrator on 11/13/2015.
 */
public class PostAddDTO {
    public int getVideoID() {
        return videoID;
    }

    public void setVideoID(int videoID) {
        this.videoID = videoID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private int videoID;
    private String description;
    private String location;
    private String userID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
