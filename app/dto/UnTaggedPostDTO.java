package dto;

import java.util.List;

/**
 * Created by Administrator on 11/13/2015.
 */
public class UnTaggedPostDTO {
    private List<String> userIDs;
    private String postID;
    private String postUserID;

    public List<String> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(List<String> userIDs) {
        this.userIDs = userIDs;
    }

    public String getPostUserID() {
        return postUserID;
    }

    public void setPostUserID(String postUserID) {
        this.postUserID = postUserID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }
}
