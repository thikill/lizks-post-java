package dto;

/**
 * Created by Administrator on 11/13/2015.
 */
public class PostDeleteDTO {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    private String id;
    private String userID;
}
