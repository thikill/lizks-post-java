package dto;

import java.util.UUID;

/**
 * Created by Administrator on 11/13/2015.
 */
public class CommentAddDTO {
    private String parentID;
    private String comment;
    private String userID;

    public String getParentID() {
        return parentID;
    }

    public void setParentID(String parentID) {
        this.parentID = parentID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
