package dto;

/**
 * Created by Administrator on 11/13/2015.
 */
public class UnDislikePostDTO {
    private String userID;
    private String postID;
    private String postUserID;

    public String getPostUserID() {
        return postUserID;
    }

    public void setPostUserID(String postUserID) {
        this.postUserID = postUserID;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }
}
