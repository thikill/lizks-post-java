package dao;

import com.datastax.driver.core.RegularStatement;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.querybuilder.Batch;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Update;
import dto.TaggedPostDTO;
import dto.UnTaggedPostDTO;
import model.UserUDT;

import java.util.*;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.putAll;

/**
 * Created by Administrator on 11/13/2015.
 */
public class TaggedDAO extends BaseDAO {
    public void tagged(TaggedPostDTO taggedPostDTO) {
        connect();
        Map<String, UDTValue> mapLikeUsers = new HashMap<>();
        List<String> userIDs = taggedPostDTO.getUserIDs();
        for (String userID : userIDs) {
            UserUDT user = UserDAO.getUserByID(userID);
            UDTValue udtValue = getUDTValueByUser(user);
            mapLikeUsers.put(userID, udtValue);
        }
        Update.Where update = QueryBuilder.update("lizks", "post")
                .with(putAll("tagged_users", mapLikeUsers))
                .where(eq("id", UUID.fromString(taggedPostDTO.getPostID()))).and(eq("user_id", taggedPostDTO.getPostUserID()));
        session.execute(update);
        close();
    }

    public void unTagged(UnTaggedPostDTO unTaggedPostDTO) {
        connect();
        List<String> userIDs = unTaggedPostDTO.getUserIDs();
        List<Delete.Where> deletes = new ArrayList<>();
        for (String userID : userIDs) {
            UserUDT user = UserDAO.getUserByID(userID);
            UDTValue udtValue = getUDTValueByUser(user);
            Delete.Where delete = QueryBuilder.delete().mapElt("tagged_users", userID).from("lizks", "post")
                    .where(eq("id", UUID.fromString(unTaggedPostDTO.getPostID()))).and(eq("user_id", unTaggedPostDTO.getPostUserID()));
            deletes.add(delete);
        }

        try {
            Batch batch;
            batch = QueryBuilder.batch(deletes.toArray(new RegularStatement[deletes.size()]));
            ResultSetFuture future = session.executeAsync(batch);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        close();
    }


}
