package dao;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import model.Post;
import model.UserUDT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;

/**
 * Created by Administrator on 11/10/2015.
 */
public class BaseDAO {
    protected Cluster cluster;
    protected Session session;
    public static final String NODE = "127.0.0.1";

    public void connect() {
        cluster = Cluster.builder()
                .addContactPoint(NODE)
                .build();
        Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n",
                metadata.getClusterName());
        for (Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n",
                    host.getDatacenter(), host.getAddress(), host.getRack());

        }
        session = cluster.connect();
    }

    public void close() {
        cluster.close();
    }

    protected UDTValue getUDTValueByUser(UserUDT user) {
        UserType userUDT = session.getCluster()
                .getMetadata().getKeyspace("lizks").getUserType("user");
        UDTValue udtValue = userUDT.newValue().setString("user_id", user.getUserID())
                .setString("email", user.getEmail())
                .setString("user_name", user.getUserName());
        return udtValue;
    }
}

