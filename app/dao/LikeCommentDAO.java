package dao;

import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Update;
import dto.*;
import model.UserUDT;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.putAll;

/**
 * Created by Administrator on 11/13/2015.
 */
public class LikeCommentDAO extends BaseDAO {

    public void like(LikeCommentDTO likeCommentDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(likeCommentDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Map<String, UDTValue> mapLikeUsers = new HashMap<>();
        mapLikeUsers.put(likeCommentDTO.getUserID(), udtValue);
        Update.Where update = QueryBuilder.update("lizks", "comment")
                .with(putAll("like_users", mapLikeUsers))
                .where(eq("id", UUID.fromString(likeCommentDTO.getCommentID()))).and(eq("user_id", likeCommentDTO.getCommentUserID()));
        session.execute(update);
        close();
    }

    public void dislike(DislikeCommentDTO disLikeCommentDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(disLikeCommentDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Map<String, UDTValue> mapLikeUsers = new HashMap<>();
        mapLikeUsers.put(disLikeCommentDTO.getUserID(), udtValue);
        Update.Where update = QueryBuilder.update("lizks", "comment")
                .with(putAll("dislike_users", mapLikeUsers))
                .where(eq("id", UUID.fromString(disLikeCommentDTO.getCommentID()))).and(eq("user_id", disLikeCommentDTO.getCommentUserID()));
        session.execute(update);
        close();
    }

    public void unLike(UnLikeCommentDTO unLikeCommentDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(unLikeCommentDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Delete.Where delete = QueryBuilder.delete().mapElt("like_users", unLikeCommentDTO.getUserID()).from("lizks", "comment")
                .where(eq("id", UUID.fromString(unLikeCommentDTO.getCommentID()))).and(eq("user_id", unLikeCommentDTO.getCommentUserID()));
        session.execute(delete);

        close();
    }

    public void unDisLike(UnDislikeCommentDTO unDisLikeCommentDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(unDisLikeCommentDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Delete.Where delete = QueryBuilder.delete().mapElt("dislike_users", unDisLikeCommentDTO.getUserID()).from("lizks", "comment")
                .where(eq("id", UUID.fromString(unDisLikeCommentDTO.getCommentID()))).and(eq("user_id", unDisLikeCommentDTO.getCommentUserID()));
        session.execute(delete);
        close();
    }



}
