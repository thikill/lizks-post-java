package dao;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import model.UserUDT;

import play.Configuration;
import play.api.Play;

import java.sql.*;

/**
 * Created by Administrator on 11/13/2015.
 */
public class UserDAO {

    public static  UserUDT getUserByID(String userID) {
        UserUDT user = null;
        // JDBC driver name and database URL
        Config conf = ConfigFactory.load();
        String  JDBC_DRIVER = conf.getString("mysql.driver");
        String DB_URL = conf.getString("mysql.url");

        //  Database credentials
         String USER = conf.getString("mysql.username");
        String PASS = conf.getString("mysql.password");

        Connection conn = null;
        Statement stmt = null;
        try{
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            stmt = conn.createStatement();
            String sql;
            sql = "SELECT * FROM account where id = \"" + userID + "\"";
            ResultSet rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set
            while(rs.next()){
                //Retrieve by column name
                String id  = rs.getString("id");
                String email = rs.getString("email");
                String userName = rs.getString("username");
                user = new UserUDT(id, email, userName);

            }
            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }// nothing we can do
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
        return user;
    }
}
