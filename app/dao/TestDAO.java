package dao;


import dto.*;
import model.Post;
import model.UserUDT;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 11/12/2015.
 */
public class TestDAO {
    public static void main(java.lang.String[] args) {
        PostDAO client = new PostDAO();
        Post post = new Post();
        post.setVideoID(4);
        post.setDescription("post desc");
        post.setLocation("Ha Noi");
        UserUDT user = new UserUDT("01", "thimb201@gmail.com", "thimb");
        // post.setPostUser(user);
        // client.insertPost();
        //  client.updatePost();
        // post = client.getPostByVideoID(2);
        // List<Post> posts = client.getPostsByUser("thi");

       // String json = gson.toJson(post);
        //1.1 get user

        //UserUDT userTest = UserDAO.getUserByID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        //1. add post
        PostAddDTO postDTO = new PostAddDTO();
        postDTO.setVideoID(4);
        postDTO.setDescription("post desc");
        postDTO.setLocation("Ha Noi");
        postDTO.setUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        //client.add(postDTO);
        //2. update
        PostUpdateDTO postUpdateDTO = new PostUpdateDTO();
        postUpdateDTO.setId("937795a0-89b2-11e5-a16c-87376e5b67ce");
        postUpdateDTO.setLocation("Ha Noi");
        postUpdateDTO.setDescription("update post");
        postUpdateDTO.setUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        //client.update(postUpdateDTO);
        //3.delete
        //4.query post
        //5.like post
        LikePostDTO likePostDTO = new LikePostDTO();
        likePostDTO.setPostID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        likePostDTO.setPostUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        likePostDTO.setUserID("2");
        LikePostDAO likeDAO = new LikePostDAO();
        //likeDAO.like(likePostDTO);
        //6. unlike
        UnLikePostDTO unLikePostDTO = new UnLikePostDTO();
        unLikePostDTO.setPostID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        unLikePostDTO.setPostUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        unLikePostDTO.setUserID("2");
        //likeDAO.unLike(unLikePostDTO);
        //7. Tagged user
        TaggedPostDTO taggedPostDTO = new TaggedPostDTO();
        taggedPostDTO.setPostID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        taggedPostDTO.setPostUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        List<java.lang.String> userIDs = new ArrayList<>();
        userIDs.add("1");
        userIDs.add("2");
        taggedPostDTO.setUserIDs(userIDs);
        TaggedDAO taggedDAO = new TaggedDAO();
        //taggedDAO.tagged(taggedPostDTO);
        //8. UnTagged user
        UnTaggedPostDTO unTaggedPostDTO = new UnTaggedPostDTO();
        unTaggedPostDTO.setPostID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        unTaggedPostDTO.setPostUserID("3b3006c5-0ae2-440c-8a56-36e3bc820f05");
        unTaggedPostDTO.setUserIDs(userIDs);
        //taggedDAO.unTagged(unTaggedPostDTO);
        //9. add comment
        CommentAddDTO commentAddDTO = new CommentAddDTO();
        commentAddDTO.setUserID("1");
        commentAddDTO.setComment("comment");
        commentAddDTO.setParentID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        CommentDAO commentDAO = new CommentDAO();
        //commentDAO.add(commentAddDTO);
        //10. update comment
        CommentUpdateDTO commentUpdateDTO = new CommentUpdateDTO();
        commentUpdateDTO.setUserID("1");
        commentUpdateDTO.setComment("comment u");
        commentUpdateDTO.setParentID("937795a0-89b2-11e5-a16c-87376e5b67ce");
        commentUpdateDTO.setId("46be3730-8cf7-11e5-9f49-85b209696035");
        commentDAO.update(commentUpdateDTO);
        int i = 0;
    }
}
