package dao;

import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.datastax.driver.core.querybuilder.Delete;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Update;
import dto.DislikePostDTO;
import dto.LikePostDTO;
import dto.UnDislikePostDTO;
import dto.UnLikePostDTO;
import model.UserUDT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.datastax.driver.core.querybuilder.QueryBuilder.*;

/**
 * Created by Administrator on 11/13/2015.
 */
public class LikePostDAO extends BaseDAO {
    public void like(LikePostDTO likePostDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(likePostDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Map<String, UDTValue> mapLikeUsers = new HashMap<>();
        mapLikeUsers.put(likePostDTO.getUserID(), udtValue);
        Update.Where update = QueryBuilder.update("lizks", "post")
                .with(putAll("like_users", mapLikeUsers))
                .where(eq("id", UUID.fromString(likePostDTO.getPostID()))).and(eq("user_id", likePostDTO.getPostUserID()));
        session.execute(update);
        close();
    }

    public void dislike(DislikePostDTO disLikePostDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(disLikePostDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Map<String, UDTValue> mapLikeUsers = new HashMap<>();
        mapLikeUsers.put(disLikePostDTO.getUserID(), udtValue);
        Update.Where update = QueryBuilder.update("lizks", "post")
                .with(putAll("dislike_users", mapLikeUsers))
                .where(eq("id", UUID.fromString(disLikePostDTO.getPostID()))).and(eq("user_id", disLikePostDTO.getPostUserID()));
        session.execute(update);
        close();
    }

    public void unLike(UnLikePostDTO unLikePostDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(unLikePostDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Delete.Where delete = QueryBuilder.delete().mapElt("like_users", unLikePostDTO.getUserID()).from("lizks", "post")
                .where(eq("id", UUID.fromString(unLikePostDTO.getPostID()))).and(eq("user_id", unLikePostDTO.getPostUserID()));
        session.execute(delete);

        close();
    }

    public void unDisLike(UnDislikePostDTO unDisLikePostDTO) {
        connect();
        UserUDT user = UserDAO.getUserByID(unDisLikePostDTO.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Delete.Where delete = QueryBuilder.delete().mapElt("dislike_users", unDisLikePostDTO.getUserID()).from("lizks", "post")
                .where(eq("id", UUID.fromString(unDisLikePostDTO.getPostID()))).and(eq("user_id", unDisLikePostDTO.getPostUserID()));
        session.execute(delete);
        close();
    }



}
