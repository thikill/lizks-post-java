package dao;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import model.Post;
import model.UserUDT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;

/**
 * Created by Administrator on 11/10/2015.
 */
public class SimpleClient {
    private Cluster cluster;
    private Session session;
    public static final String NODE = "127.0.0.1";

    public void connect() {
        cluster = Cluster.builder()
                .addContactPoint(NODE)
                .build();
        Metadata metadata = cluster.getMetadata();
        System.out.printf("Connected to cluster: %s\n",
                metadata.getClusterName());
        for (Host host : metadata.getAllHosts()) {
            System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n",
                    host.getDatacenter(), host.getAddress(), host.getRack());

        }
        session = cluster.connect();
    }

    public void close() {
        cluster.close();
    }

    public void insertPost() {
        connect();
        session.execute("insert into lizks.post(id,video_id, description) values(now(),2, 'description');");
        close();

    }

    public void updatePost() {
        connect();
        session.execute("UPDATE lizks.post\n" +
                "  SET dislike_users =  {'1':('user_id','email','user_name' )}\n" +
                "  WHERE id=09918730-8794-11e5-8dd6-53998a42f1bd\n" +
                "  and video_id=2;");
        close();

    }


    public Post getPostByVideoID(int videoID) {
        connect();
        Post post = new Post();

        Select.Where select = QueryBuilder.select()
                .from("lizks", "post").allowFiltering()
                .where(eq("video_id", videoID));
        ResultSet results = session.execute(select);
        for (Row row : results) {
            //1.0 get post
            post.setVideoID(videoID);
            post.setId(row.getUUID("id"));
            post.setCreateAt(row.getDate("create_at"));
            post.setDescription(row.getString("description"));
            post.setLocation(row.getString("location"));
            post.setUpdateAt(row.getDate("update_at"));
            //1.1 get Post user
            UDTValue udtValue = row.getUDTValue("post_user");
            post.setPostUser(getUserByUDT(udtValue));
            //1.2 get dis like users
            post.setLikeUsers(getUsersByMapColumn(row, "like_users"));
            //1.3 get dis like users
            post.setDislikeUsers(getUsersByMapColumn(row, "dislike_users"));
            //1.4 get tagged users
            post.setTagUsers(getUsersByMapColumn(row, "tagged_users"));

            int a = 0;
        }
        close();
        return post;
    }

    private UserUDT getUserByUDT(UDTValue udtValue) {
        String userID = udtValue.getString("user_id");
        String email = udtValue.getString("email");
        String userName = udtValue.getString("user_name");
        UserUDT user = new UserUDT(userID, email, userName);
        return user;
    }

    private List<UserUDT> getUsersByMapColumn(Row row, String columnName){
        List<UserUDT> users = new ArrayList<>();
        Map<String, UDTValue> mapDislikeUsers = row.getMap(columnName, String.class, UDTValue.class);
        for (Map.Entry<String, UDTValue> entry : mapDislikeUsers.entrySet()) {
            UDTValue udtValue = entry.getValue();
            users.add(getUserByUDT(udtValue));
        }
        return users;
    }

    public static void main(String[] args) {
        SimpleClient client = new SimpleClient();
        // client.insertPost();
        //  client.updatePost();
        Post post = client.getPostByVideoID(2);
        int i =0;
    }
}

