package dao;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.*;
import com.datastax.driver.core.utils.UUIDs;
import dto.PostAddDTO;
import dto.PostDeleteDTO;
import dto.PostUpdateDTO;
import model.Comment;
import model.Post;
import model.UserUDT;

import java.util.*;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.set;

/**
 * Created by Administrator on 11/10/2015.
 */
public class PostDAO extends BaseDAO {


    public void add(PostAddDTO post) {
        connect();
        UserUDT user = UserDAO.getUserByID(post.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Insert insert = QueryBuilder.insertInto("lizks", "post").value("id", UUIDs.timeBased())
                .value("user_id", post.getUserID())
                .value("video_id", post.getVideoID())
                .value("description", post.getDescription())
                .value("location", post.getLocation())
                .value("create_at", new Date())
                .value("post_user", udtValue);

        session.execute(insert);
        close();

    }

    public void update(PostUpdateDTO post) {
        connect();
        Update.Where update = QueryBuilder.update("lizks", "post").with(set("description", post.getDescription()))
                .and(set("location", post.getLocation()))
                .and(set("update_at", new Date()))
                .where(eq("id", UUID.fromString(post.getId()))).and(eq("user_id", post.getUserID()));
        session.execute(update);
        close();
    }

    public void delete(PostDeleteDTO post) {
        connect();
        Delete.Where delete = QueryBuilder.delete().from("lizks", "post").where(eq("id", UUID.fromString(post.getId()))).and(eq("user_id", post.getUserID()));
        session.execute(delete);
        close();
    }


    public Post getPostByID(int postID) {
        connect();
        Post post = new Post();

        Select.Where select = QueryBuilder.select()
                .from("lizks", "post").allowFiltering()
                .where(eq("id", postID));
        ResultSet results = session.execute(select);
        for (Row row : results) {
            //1.0 get post
            post.setVideoID(row.getInt("video_id"));
            post.setId(row.getUUID("id"));
            post.setCreateAt(row.getDate("create_at"));
            post.setDescription(row.getString("description"));
            post.setLocation(row.getString("location"));
            post.setUpdateAt(row.getDate("update_at"));
            //1.1 get Post user
            UDTValue udtValue = row.getUDTValue("post_user");
            post.setPostUser(getUserByUDT(udtValue));
            //1.2 get like users
            post.setLikeUsers(getUsersByMapColumn(row, "like_users"));
            //1.3 get dislike users
            post.setDislikeUsers(getUsersByMapColumn(row, "dislike_users"));
            //1.4 get tagged users
            post.setTagUsers(getUsersByMapColumn(row, "tagged_users"));
            //1.5 get comments
            post.setComments(getComments(post.getId()));
            int a = 0;
        }
        close();
        return post;
    }

    public List<Post> getPostsByUser(String userID) {
        connect();
        List<Post> posts = new ArrayList<>();
        Select.Where select = QueryBuilder.select()
                .from("lizks", "post").allowFiltering()
                .where(eq("user_id", userID));
        ResultSet results = session.execute(select);
        for (Row row : results) {
            posts.add(getPostByID(row.getInt("id")));
        }
        close();
        return posts;

    }

    public List<Comment> getComments(UUID parentID) {
        List<Comment> comments = new ArrayList<>();
        Select.Where select = QueryBuilder.select()
                .from("lizks", "comment").allowFiltering()
                .where(eq("parent_id", parentID));
        ResultSet results = session.execute(select);
        Comment comment = new Comment();
        for (Row row : results) {
            //1.0 get comment
            comment.setParentID(parentID);
            comment.setId(row.getUUID("id"));
            comment.setComment(row.getString("comment"));
            comment.setCreateAt(row.getDate("create_at"));
            comment.setUpdateAt(row.getDate("update_at"));
            //1.2 get like users
            comment.setLikeUsers(getUsersByMapColumn(row, "like_users"));
            //1.3 get dislike users
            comment.setDislikeUsers(getUsersByMapColumn(row, "dislike_users"));
            //1.4 get comment user
            UDTValue udtValue = row.getUDTValue("comment_user");
            if (udtValue != null)
                comment.setCommentUser(getUserByUDT(udtValue));
            //1.5 get child comments
            comment.setComments(getComments(comment.getId()));
            comments.add(comment);
        }
        return comments;
    }

    private UserUDT getUserByUDT(UDTValue udtValue) {
        String userID = udtValue.getString("user_id");
        String email = udtValue.getString("email");
        String userName = udtValue.getString("user_name");
        UserUDT user = new UserUDT(userID, email, userName);
        return user;
    }


    private List<UserUDT> getUsersByMapColumn(Row row, String columnName) {
        List<UserUDT> users = new ArrayList<>();
        Map<String, UDTValue> mapDislikeUsers = row.getMap(columnName, String.class, UDTValue.class);
        for (Map.Entry<String, UDTValue> entry : mapDislikeUsers.entrySet()) {
            UDTValue udtValue = entry.getValue();
            users.add(getUserByUDT(udtValue));
        }
        return users;
    }

}

