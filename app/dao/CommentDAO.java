package dao;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.querybuilder.*;
import com.datastax.driver.core.utils.UUIDs;
import dto.*;
import model.Comment;
import model.Post;
import model.UserUDT;

import java.util.*;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.set;

/**
 * Created by Administrator on 11/10/2015.
 */
public class CommentDAO extends BaseDAO {


    public void add(CommentAddDTO dto) {
        connect();
        UserUDT user = UserDAO.getUserByID(dto.getUserID());
        UDTValue udtValue = getUDTValueByUser(user);
        Insert insert = QueryBuilder.insertInto("lizks", "comment").value("id", UUIDs.timeBased())
                .value("parent_id",  UUID.fromString(dto.getParentID()))
                .value("user_id", dto.getUserID())
                .value("comment", dto.getComment())
                .value("create_at", new Date())
                .value("comment_user", udtValue);
        session.execute(insert);
        close();

    }

    public void update(CommentUpdateDTO dto) {
        connect();
        Update.Where update = QueryBuilder.update("lizks", "comment").with(set("comment", dto.getComment()))
                .and(set("update_at", new Date()))
                .where(eq("id", UUID.fromString(dto.getId()))).and(eq("parent_id", UUID.fromString(dto.getParentID())))
                .and(eq("user_id", dto.getUserID()));
        session.execute(update);
        close();
    }

    public void delete(CommentDeleteDTO dto) {
        connect();
        Delete.Where delete = QueryBuilder.delete().from("lizks", "comment")
                .where(eq("id", UUID.fromString(dto.getId()))).and(eq("parent_id", UUID.fromString(dto.getParentID())))
                .and(eq("user_id", dto.getUserID()));
        session.execute(delete);
        close();
    }

}

