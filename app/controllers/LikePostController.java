package controllers;

import com.google.inject.Inject;
import dao.LikePostDAO;
import dto.*;
import play.mvc.Result;
import utils.ProcessStatus;

/**
 * Created by Administrator on 11/16/2015.
 */
public class LikePostController extends  BaseController {
    @Inject
    LikePostDAO dao;

    public Result like() {
        LikePostDTO dto = validate(LikePostDTO.class);
        if (dto != null) {
            dao.like(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result dislike() {
         DislikePostDTO dto = validate(DislikePostDTO.class);
        if (dto != null) {
            dao.dislike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result unLike() {
        UnLikePostDTO dto = validate(UnLikePostDTO.class);
        if (dto != null) {
            dao.unLike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result unDisLike() {
        UnDislikePostDTO dto = validate(UnDislikePostDTO.class);
        if (dto != null) {
            dao.unDisLike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }




}
