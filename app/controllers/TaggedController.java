package controllers;

import com.google.inject.Inject;
import dao.LikePostDAO;
import dao.TaggedDAO;
import dto.LikePostDTO;
import dto.TaggedPostDTO;
import dto.UnTaggedPostDTO;
import play.mvc.Result;
import utils.ProcessStatus;

/**
 * Created by Administrator on 11/16/2015.
 */
public class TaggedController extends  BaseController {
    @Inject
    TaggedDAO dao;

    public Result tagged() {
        TaggedPostDTO dto = validate(TaggedPostDTO.class);
        if (dto != null) {
            dao.tagged(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result unTagged() {
        UnTaggedPostDTO dto = validate(UnTaggedPostDTO.class);
        if (dto != null) {
            dao.unTagged(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }
}
