package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import dao.PostDAO;
import dto.PostAddDTO;

import dto.PostDeleteDTO;
import dto.PostUpdateDTO;
import play.api.libs.json.JsValue;
import play.mvc.Result;
import utils.ProcessStatus;
import views.html.index;
import play.mvc.Result.*;
/**
 * Created by Administrator on 11/16/2015.
 */
public class PostController extends  BaseController {
    @Inject
    PostDAO dao;
    public Result add() {
        PostAddDTO dto = validate(PostAddDTO.class);
        if (dto != null) {
            dao.add(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result edit() {
        PostUpdateDTO dto = validate(PostUpdateDTO.class);
        if (dto != null) {
            dao.update(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result delete() {
        PostDeleteDTO dto = validate(PostDeleteDTO.class);
        if (dto != null) {
            dao.delete(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }


}
