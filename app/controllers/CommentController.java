package controllers;

import com.google.inject.Inject;
import dao.CommentDAO;
import dao.CommentDAO;
import dto.CommentAddDTO;
import dto.CommentDeleteDTO;
import dto.CommentUpdateDTO;
import play.mvc.Result;
import utils.ProcessStatus;

/**
 * Created by Administrator on 11/16/2015.
 */
public class CommentController extends  BaseController {
    @Inject
    CommentDAO dao;
    public Result add() {
        CommentAddDTO dto = validate(CommentAddDTO.class);
        if (dto != null) {
            dao.add(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result edit() {
        CommentUpdateDTO dto = validate(CommentUpdateDTO.class);
        if (dto != null) {
            dao.update(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result delete() {
        CommentDeleteDTO dto = validate(CommentDeleteDTO.class);
        if (dto != null) {
            dao.delete(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }


}
