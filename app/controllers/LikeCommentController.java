package controllers;

import com.google.inject.Inject;
import dao.LikeCommentDAO;
import dto.DislikeCommentDTO;
import dto.LikeCommentDTO;
import dto.UnDislikeCommentDTO;
import dto.UnLikeCommentDTO;
import play.mvc.Result;
import utils.ProcessStatus;

/**
 * Created by Administrator on 11/16/2015.
 */
public class LikeCommentController extends  BaseController {
    @Inject
    LikeCommentDAO dao;

    public Result like() {
        LikeCommentDTO dto = validate(LikeCommentDTO.class);
        if (dto != null) {
            dao.like(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result dislike() {
         DislikeCommentDTO dto = validate(DislikeCommentDTO.class);
        if (dto != null) {
            dao.dislike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result unLike() {
        UnLikeCommentDTO dto = validate(UnLikeCommentDTO.class);
        if (dto != null) {
            dao.unLike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }

    public Result unDisLike() {
        UnDislikeCommentDTO dto = validate(UnDislikeCommentDTO.class);
        if (dto != null) {
            dao.unDisLike(dto);
            return status(ProcessStatus.OK);
        } else {
            return status(ProcessStatus.ERROR);
        }
    }
}
