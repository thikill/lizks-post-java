package model;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Administrator on 11/10/2015.
 */
public class Comment {

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getParentID() {
        return parentID;
    }

    public void setParentID(UUID parentID) {
        this.parentID = parentID;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public UserUDT getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(UserUDT commentUser) {
        this.commentUser = commentUser;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public List<UserUDT> getLikeUsers() {
        return likeUsers;
    }

    public void setLikeUsers(List<UserUDT> likeUsers) {
        this.likeUsers = likeUsers;
    }

    public List<UserUDT> getDislikeUsers() {
        return dislikeUsers;
    }

    public void setDislikeUsers(List<UserUDT> dislikeUsers) {
        this.dislikeUsers = dislikeUsers;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    private UUID id;
    private UUID parentID;
    private String comment;
    private UserUDT commentUser;
    private Date createAt;
    private Date updateAt;
    private List<UserUDT> likeUsers;
    private List<UserUDT> dislikeUsers;
    private List<Comment> comments;

}
