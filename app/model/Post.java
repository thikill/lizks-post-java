package model;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.Frozen;
import com.datastax.driver.mapping.annotations.Table;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Administrator on 11/10/2015.
 */

public class Post {


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getVideoID() {
        return videoID;
    }

    public void setVideoID(int videoID) {
        this.videoID = videoID;
    }



    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserUDT> getLikeUsers() {
        return likeUsers;
    }

    public void setLikeUsers(List<UserUDT> likeUsers) {
        this.likeUsers = likeUsers;
    }

    public List<UserUDT> getDislikeUsers() {
        return dislikeUsers;
    }

    public void setDislikeUsers(List<UserUDT> dislikeUsers) {
        this.dislikeUsers = dislikeUsers;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public UserUDT getPostUser() {
        return postUser;
    }

    public void setPostUser(UserUDT postUser) {
        this.postUser = postUser;
    }

    public List<UserUDT> getTagUsers() {
        return tagUsers;
    }

    public void setTagUsers(List<UserUDT> tagUsers) {
        this.tagUsers = tagUsers;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    private UUID id;
    private int videoID;
    private List<UUID> commentIDs;
    private Date createAt;
    private String description;
    private List<UserUDT> likeUsers;
    private List<UserUDT> dislikeUsers;
    private String location;
    private UserUDT postUser;
    private List<UserUDT> tagUsers;
    private Date updateAt;
    private String userID;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<UUID> getCommentIDs() {
        return commentIDs;
    }

    public void setCommentIDs(List<UUID> commentIDs) {
        this.commentIDs = commentIDs;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    private List<Comment> comments;




}
